//
//  ViewController.swift
//  zasoby
//
//  Created by Jakub Krasnowski on 01/08/2017.
//  Copyright © 2017 Jakub Krasnowski. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var ref: DatabaseReference?
    var databaseHandle:DatabaseHandle = 0
    
    var postData = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        //Retrieve the posts and listen to changes
        ref?.child("Posts").observe(.childAdded, with: { (snapshot) in
            
            // Code to execute when a child is added under "Posts"
            
        //take the value fromthe snapshot and added it to the postData array
            // Try to convert the value of data to string
            let post = snapshot.value as? String
            
            if let actualPost = post {
                // Append the data to our postData array
                self.postData.append(actualPost)
                //Reload the tableView
                self.tableView.reloadData()
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell")
        cell?.textLabel?.text = postData[indexPath.row]

        return cell!
}
}
